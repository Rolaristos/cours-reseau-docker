#!/usr/bin/python3
import socket

def client(host, port):
    sock = socket.socket()
    sock.connect((host, port))
    f = sock.makefile(mode="rw")
    
    while True :
        saisie = str(input("Saisir get/quit/incr/decr/add N\n"))

        if (saisie == "get"):
            f.write("get\n")
            f.flush()
            print(f.readline(), end=" ")
            
        elif saisie == "incr":
            f.write("incr\n")
            f.flush()
            print(f.readline(), end=" ")
            
        elif saisie == "decr":
            f.write("decr\n")
            f.flush()
            print(f.readline(), end=" ")
            
        elif saisie[0:3] == "add":
            f.write(saisie+"\n")
            f.flush()
            print(f.readline(), end=" ")
        
        elif (saisie == "quit"):
            f.write("quit\n")
            f.flush()
            print(f.readline(), end=" ")
            break
        
        else:
            print("Saisie non conforme au choix établi.")
        
    f.close()
    sock.shutdown(socket.SHUT_RDWR)
    sock.close()

client("localhost", 4000)
