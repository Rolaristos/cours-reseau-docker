# Prise de note cours réseau

## Séance 1  
  
- Rappel :  
    - ICMP Internet Control Message Protocol -> protocole utilisé pour le ping entre machines  
    - Les paquets à installer dans les nouveaux environnements :  
        - ```
        apk add scapy
        apk add tcpdump
        pip install ipython
        ```  
    - 1: who as, 2: is at   

- `lo` signifie LOOPBACK. Apparaît après avoir fait la commande `ip address`:  
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1  
  
- `apk add scapy` permet d'installer toute la librairie  
  
- pour installer python : `pip install ipython` et pour ouvrir `ipython3`  
  
- `paquets = sniff(iface="eth0")` permet d'écouter, et avant cela, ne pas oublier de faire `from scapy.all import *`  
  
- `arping [adresse à ping]` permet de ping.  
  
- `paquets.summary()` nous résumes nos paquets. Voir à quoi ça ressemble dans TP1. CODE: 7456
  
- Un paquet est constitué d'un Ethernet qui englobe ARP. Visualiser un grand carré Ethernet, dans lequel il y a un autre carré ARP.  
    On peut affecter le premier élément d'un paquet (voir paquet comme une liste) de la façon suivante : paquets[0]  
  
En faisant simplement `p` dans l'interpréteur python, nous obtenons un affichage à peu près comme cela :  
  
```
<Ether  dst=ff:ff:ff:ff:ff:ff src=f2:92:a8:5c:4d:4f type=ARP |<ARP  hwtype=Ethernet (10Mb) ptype=IPv4 hwlen=6 plen=4 op=who-has hwsrc=f2:92:a8:5c:4d:4f psrc=192.168.0.27 hwdst=ff:ff:ff:ff:ff:ff pdst=192.168.0.28 |>>
```
  
    - Nous avons donc dans ether, un dst, type, ect...  
  
    - ARP dans l'affichage indique simplement le protocole utilisé.  
  
Les champs qui nous intéressent sont `op` et `psrc`  
le dernier correspond à l'adresse ip de la source.  
  
Maintenant si nous faisons `p[0]` nous obtiendrons le même affichage. Mais en faisant `p[1]` nous pourrions avoir l'ARP par exemple. Cela est aussi possible avec le nom directement, comme pour la clé d'un dictionnaire.  
    
On peut spécifier une fonction python de la manière suivante lors du sniff :  
`sniff(prn=affiche_paquet, iface="eth0")`  
  
Voir la méthode `affiche_paquet` dans fonctions.py.  
CODE: 0001
  
# Créer un paquet:  
CODE: 0002  
  
`paquet = Ether(src='f2:92:a8:5c:4d:4f', dst='b6:c4:9a:ea:44:01')`  
 
Maintenant pour l'envoyer, il suffit de taper :  
- `sendp(paquet ,iface='eth0')` à noter que `iface` représente l'interface par laquelle on passe.  
  
# Adresse IP fantôme, usurpation
  
Rappel des ip :  
- 192.168.128.22 -> adresse IP fantôme 
- 192.168.0.28 -> adresse IP machine 1 
- 192.168.0.27 -> adresse IP machine 2
  
Voir la fonction `reponse_fantome(paquet)`  
  
## Séance 2

(Dédiée à GNS3)

## Séance 3  

### Le NAT
Un réseau privé est une réseau non routés sur internet.  
  
Les adresses réservées aux réseaux privés sont les suivantes :  
- 10.0.0.0/8
- 172.16.0.0/12
- 192.168.0.0/16

rappel :  
- Quand un paquet passe par un routeur, son ttl passe à 63.   
- Si nous n'avons pas de réponse lors d'un ping, c'est qu'un problème est survenu lors du retour.  

![Alt text](r1.png)  
  
Ici, pc1 ne reçois pas de message d'erreur quand on ping pc3. Or dans le sens inverse, nous obtenons ce message plus clair :  
*1.2.3.3 icmp_seq=1 ttl=255 time=9.789 ms (ICMP type:3, code:1, Destination host unreachable)  
  
`ip nat inside source list 1 pool lana`

nat : on dit qu'on fait du NAT  
inside : On définit le inside  
source : On lui envoie en quelque sorte la liste crée  
  
(info) :  
- Config term
- ip nat pool lana 64.64.64.65 64.64.64.126 netmask 255.255.255.192  
- access-list 1 permit 192.168.0.0 0.0.0.255
  
En faisant cela :  

R1(config)#interface e0/0
R1(config-if)#ip nat inside

On indique à l'interface e0/0 que c'est le privé, et à e0/1 qu'il est publique :  
  
R1(config)#interface e0/1
R1(config-if)#ip nat outside

Il ne reste plus qu'à ajouter la route suivante au routeur 2 :  
- ip route 64.64.64.64 255.255.255.192 1.2.3.254  
  
Les requêtes ne fonctionnent que du privé à l'extérieur. Pas l'inverse.  
Le privé peut contacter l'extérieur et recevoir une réponse, mais l'initiative ne peut provenir de l'extérieur.  
  
Voici le chronogramme :  
  
![Alt text](chronogramme.png)  
  
### Le NAT/PAT  
  
`ip nat inside source list 1 interface e0/1 overload`  
  
overload permet de surcharger les adresses publique.

## Séance 4  
  
`sock.recvfrom(BUFSIZE)` renvoie simplement un tuple (data, adresse de retour)  
`encode()` permet d'encoder en binaire la data.  
`bind` sert à assigner un socket à une adresse et un port.  

## Séance 5

`self.file.write("quit\n")`: Cette ligne écrit la chaîne "quit" suivie d'un saut de ligne dans le fichier associé à l'objet self.file.

`self.file.flush()`: Cette ligne force l'écriture immédiate des données dans le fichier, garantissant que les modifications sont effectivement enregistrées.

`socket.accept()`: Cette ligne attend une connexion entrante sur le socket. Lorsqu'une connexion est établie, elle retourne un nouvel objet socket (cli) représentant la connexion et l'adresse (addr) de l'émetteur.
  
Les clients (ne sont jamais contactés directement), on ne spécifie pas l'adresse et le port à laquel ils sont liés. On ne précise juste pas avec `bind`. Ça se fera tout seul par le système d'exploitation.  
  
`sock.connect(("localhost", 5555))` : Établit une connexion réseau vers un serveur hébergé sur la machine locale (localhost) via le port 5555 en utilisant le socket sock. Cela permet d'initialiser une communication bidirectionnelle entre le client et le serveur.