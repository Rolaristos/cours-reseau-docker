
# Première fonction, afficher le contenu d'un paquet
#CODE: 0001
def affiche_paquet(paquets):
    print(paquets.summary())

# On crée ici notre premier paquet
#CODE: 0002
paquet = Ether(src='f2:92:a8:5c:4d:4f', dst='b6:c4:9a:ea:44:01')

#CODE: 0003
ip_fantome = "192.168.128.22"
mac_fantome = "22:b7:22:ea:44:01"

def reponse_fantome(paquet):
    if ARP in paquet and paquet[op] == 1 and paquet[pdst]=="192.168.128.22":
        paquet_fantome = Ether(src='22:b7:22:ea:44:01', dst=paquet[src])/ARP(op=2, hwsrc="22:b7:22:ea:44:01", 
                psrc="192.168.128.22", hwdst=paquet[hwsrc], pdst=paquet[psrc])
        sendp(paquet_fantome, iface='eth0')
    else:
        print('failed.')