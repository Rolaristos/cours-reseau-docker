### TP1

résultat du ip address pour la machine 1:  

```
$ ip address
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
2: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN 
    link/ether 02:42:6e:0d:31:3b brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
62165: eth0@if62166: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether b6:c4:9a:ea:44:01 brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.28/23 scope global eth0
       valid_lft forever preferred_lft forever
62169: eth1@if62170: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether 02:42:ac:12:00:ac brd ff:ff:ff:ff:ff:ff
    inet 172.18.0.172/16 scope global eth1
       valid_lft forever preferred_lft forever
```

résultat du ip address pour la machine 2:  

```
$ ip address
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
62238: eth0@if62239: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether f2:92:a8:5c:4d:4f brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.27/23 scope global eth0
       valid_lft forever preferred_lft forever
62242: eth1@if62243: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether 02:42:ac:12:00:b3 brd ff:ff:ff:ff:ff:ff
    inet 172.18.0.179/16 scope global eth1
       valid_lft forever preferred_lft forever
```
  
résultat de la commande `paquets.summary()` CODE: 7456
  
```
paquets.summary()
Ether / ARP who has 192.168.0.28 says 192.168.0.27
Ether / ARP is at b6:c4:9a:ea:44:01 says 192.168.0.28
Ether / ARP who has 192.168.0.28 says 192.168.0.27
Ether / ARP is at b6:c4:9a:ea:44:01 says 192.168.0.28
Ether / ARP who has 192.168.0.28 says 192.168.0.27
Ether / ARP is at b6:c4:9a:ea:44:01 says 192.168.0.28
```