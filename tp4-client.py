#!/usr/bin/python

import socket
import sys
BUFSIZE = 1024
def client(host, port):
    sock = socket.socket(type=socket.SOCK_DGRAM)
    #pas de bind pour le client car non nécessaire
    addr = (host, port)
    sock.sendto(sys.argv[2].encode(), addr)
    input()
    # b'' envoie une str vide transformée en binaire
    data = sock.recv(BUFSIZE)
    #recupère juste le message non l'adresse que l'on already know
    print(data.decode(), end="")

if sys.argv[2] == "date" or sys.argv[2] == "user":
    client(sys.argv[1], 5556)

